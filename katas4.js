const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function helper1(X)
{
    let newElement;
    if(X.includes('kata'))
    {
        newElement = document.createElement("p");
        newElement.textContent = X;
    }
    else
    {     
        newElement = document.createElement("div");
        newElement.textContent = JSON.stringify(X);
    }
    document.body.appendChild(newElement);
    return X;
}
function helper2(X)
{
    const newElement = document.createElement("div");
    newElement.textContent = X;
    document.body.appendChild(newElement);
    return X;
}

function kata1(a,b)
{
    helper1(a);
    helper1(b);
    return b;
}
kata1("kata1",gotCitiesCSV);
//============================================
function kata2(a,b)
{
    helper1(a);
    helper1(b);
    return b;
}
kata2("kata2",bestThing);
//============================================
helper1("kata3");
function kata3(x)
{
    const newElement = document.createElement("div");
    newElement.textContent = x.replace(/,/g, "; ");
    document.body.appendChild(newElement);
    return x;
}
kata3(gotCitiesCSV);
//=============================================
helper1("kata4");
function kata4(x)
{
    resultArray = {};
    resultArray = String(x);
    helper2(resultArray);
    return x;
}
kata4(lotrCitiesArray);
//=============================================
helper1("kata5");
function kata5(y)
{
    helper2(y.slice(0,5));
}
kata5(lotrCitiesArray);
//=============================================
helper1("kata6");
function kata6(y)
{
    helper2(y.slice(-5));
}
kata6(lotrCitiesArray);
//=============================================
helper1("kata7");
function kata7(y)
{
    helper2(y.slice(2,5));
}
kata7(lotrCitiesArray);
helper1("kata8");
function kata8(y)
{
    y.splice(2,1)
    helper2(y);
}
kata8(lotrCitiesArray);
//==============================================
helper1("kata9");
function kata9(y)
{
    y.splice(5,2);
    helper2(y);
}
kata9(lotrCitiesArray);
//===============================================
helper1("kata10");
function kata10(y)
{
    y.splice(2,0,"Rohan")
    helper2(y);
}
kata10(lotrCitiesArray);
//================================================
helper1("kata11");
function kata11(y)
{
    y.splice(5,1,"Deadest Marshes");
    helper2(y);
}
kata11(lotrCitiesArray);
//=================================================
helper1("kata12");
function kata12(y)
{
    helper2(y.slice(0,14));
}
kata12(bestThing);
//=====================================
helper1("kata13");
function kata13(y)
{
    helper2(y.slice(-12));
}
kata13(bestThing);
//============================================
helper1("kata14");
function kata14(y)
{
    helper2(y.slice(23,38));
}
kata14(bestThing);
//=============================================
helper1("kata15");
function kata15(y)
{
    helper2(y.substring(68));
}
kata15(bestThing);
//===============================================
helper1("kata16");
function kata16(y)
{
    helper2(y.substring(23,38));
}
kata16(bestThing);
//=================================================
helper1("kata17");
function kata17(y)
{
    helper2(y.indexOf("only"));
}
kata17(bestThing);
//=================================================
helper1("kata18");
function kata18(y)
{
    helper2(y.indexOf("bit"));
}
kata18(bestThing);
//====================================================
helper1("kata19");

function kata19(y)
{
    const results = [];
    const kataNineteenArray = y.split(',');
    
    for (let i = 0; i < kataNineteenArray.length; i++) 
    {
        if (kataNineteenArray[i].includes("ee")) 
        {
            results.push(kataNineteenArray[i])
        } 
        if (kataNineteenArray[i].includes("aa")) 
        {
            results.push(kataNineteenArray[i])
        }
    }        
    helper2(results);
}
kata19(gotCitiesCSV);
//================================================
helper1("kata20");
function kata20(y)
{
    function filterItems(arr, query) {
        return arr.filter(function(el) {
            return el.indexOf(query.toLowerCase()) !== -1;
        })
      }
    helper2(filterItems(y, "or"));
}
kata20(lotrCitiesArray);
//=========================================================
helper1("kata21");

function kata21(y)
{
    const results = [];
    const kataNineteenArray = y.split(' ');
    
    for (let i = 0; i < kataNineteenArray.length; i++) 
    {
        if (kataNineteenArray[i].includes("b")) 
        {
            results.push(kataNineteenArray[i])
        } 
        //results.splice()
    }        
    helper2(results);    
}
kata21(bestThing);
//========================================================
helper1("kata22");

function kata22(y)
{
    if(y.includes("Mirkwood"))
    {
        helper1("Yes");
    }
    else
    {
        helper1("No");
    }
}
kata22(lotrCitiesArray);
//============================================================
helper1("kata23");
function kata23(y)
{
    if(y.includes("Hollywood"))
    {
        helper1("Yes");
    }
    else
    {
        helper1("No");
    }
}
kata23(lotrCitiesArray);
//=============================================================
helper1("kata24");
function kata24(y)
{
    const results = y.findIndex(y => y === "Mirkwood");
    helper2(results);
}
kata24(lotrCitiesArray);
//=============================================================
helper1("kata25");/*
function kata25(y)
{
    const result = y.find(fruit => fruit === "Dead Marshes");
    helper2(y.find(result));
}
kata25(lotrCitiesArray);*/
//==============================================================
helper1("kata26");
function kata26(y)
{
    helper2(y.reverse());
}
kata26(lotrCitiesArray);
//============================================================
helper1("kata27");
function kata27(y)
{
    helper2(y.sort());
}
kata27(lotrCitiesArray);
//================================================================
helper1("kata28");
function kata28(y)
{
    y.sort((a, b) => a.length - b.length);
    helper2(y);
}
kata28(lotrCitiesArray);
//====================================================
helper1("kata29");
function kata29(y)
{
    y.pop();
    helper2(y)
}
kata29(lotrCitiesArray);
//=========================================================
helper1("kata30")
function kata30(y)
{
    y.push("Deadest Marshes");
    helper2(y)
}
kata30(lotrCitiesArray);
//=================================================
helper1("kata31");
function kata31(y)
{
    y.shift();
    helper2(y);
}
kata31(lotrCitiesArray);
//=====================================================
helper1("kata32");
function kata32(y)
{
    y.unshift("Rohan");
    helper2(y);
}
kata32(lotrCitiesArray);
